#!/usr/bin/env sh

#  Common Utilities for Matrix Shell Suite
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

WGET_PARAMS="-O- --quiet --no-config"

HGET() {
	URL="$1"
	wget $WGET_PARAMS "$URL"
}

HPOST() {
	URL="$1" && shift
	DATA=$*

	wget $WGET_PARAMS --method=POST --body-data "$DATA" "$URL"
}

exists() {
	if ! command -v "$1" >/dev/null 2>&1; then
		>&2 echo "Command $1 not found in \$PATH"
		return 1
	fi
	return 0
}

get_homeserver_url() {
	url="$1"

	# Set the https:// prefix if necessary
	url=$(echo "$url" | sed 's#^http://#https://#g')
	url=$(echo "$url" | sed '/^https/! s#^#https://#g')

	# Set the port number (8448) if not specified
	url=$(echo "$url" | sed '/:[0-9]*$/! s#$#:8448#g')

	echo "$url"
}
